package com.devcamp.s10_1.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10_1.models.School;
import com.devcamp.s10_1.services.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchools() {
        return schoolService.getAllSchools();
    }

    @GetMapping("/school-info")
    public School getSchoolById(@RequestParam(name = "id", required = true) int schoolId) {
        return schoolService.getSchoolById(schoolId);
    }

    @GetMapping("/schools-more-students/{noNumber}")
    public ArrayList<School> getSchoolsWithMoreStudents(@PathVariable int noNumber) {
        return schoolService.getSchoolsWithMoreStudents(noNumber);
    }
}
