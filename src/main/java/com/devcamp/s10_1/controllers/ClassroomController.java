package com.devcamp.s10_1.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10_1.models.Classroom;
import com.devcamp.s10_1.services.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;

    @GetMapping("/classrooms")
    public ArrayList<Classroom> getAllClassrooms() {
        return classroomService.getAllClassrooms();
    }

    @GetMapping("/classrooms-more-students/{noNumber}")
    public ArrayList<Classroom> getClassroomsWithMoreStudents(@PathVariable int noNumber) {
        return classroomService.getClassroomsWithMoreStudents(noNumber);
    }
}
