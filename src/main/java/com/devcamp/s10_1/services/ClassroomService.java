package com.devcamp.s10_1.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10_1.models.Classroom;

@Service
public class ClassroomService {
    Classroom classroom1 = new Classroom(1, "Classroom 1", 30);
    Classroom classroom2 = new Classroom(2, "Classroom 2", 25);
    Classroom classroom3 = new Classroom(3, "Classroom 3", 28);

    Classroom classroom4 = new Classroom(4, "Classroom 4", 35);
    Classroom classroom5 = new Classroom(5, "Classroom 5", 27);
    Classroom classroom6 = new Classroom(6, "Classroom 6", 29);

    Classroom classroom7 = new Classroom(7, "Classroom 7", 33);
    Classroom classroom8 = new Classroom(8, "Classroom 8", 31);
    Classroom classroom9 = new Classroom(9, "Classroom 9", 26);

    public ArrayList<Classroom> getAllClassrooms() {
        ArrayList<Classroom> allClassrooms = new ArrayList<>();

        allClassrooms.add(classroom1);
        allClassrooms.add(classroom2);
        allClassrooms.add(classroom3);
        allClassrooms.add(classroom4);
        allClassrooms.add(classroom5);
        allClassrooms.add(classroom6);
        allClassrooms.add(classroom7);
        allClassrooms.add(classroom8);
        allClassrooms.add(classroom9);

        return allClassrooms;
    }

    public ArrayList<Classroom> getClassroomsShool1() {
        ArrayList<Classroom> classroomsShool1 = new ArrayList<>();

        classroomsShool1.add(classroom1);
        classroomsShool1.add(classroom2);
        classroomsShool1.add(classroom3);

        return classroomsShool1;
    }

    public ArrayList<Classroom> getClassroomsShool2() {
        ArrayList<Classroom> classroomsShool2 = new ArrayList<>();

        classroomsShool2.add(classroom4);
        classroomsShool2.add(classroom5);
        classroomsShool2.add(classroom6);

        return classroomsShool2;
    }

    public ArrayList<Classroom> getClassroomsShool3() {
        ArrayList<Classroom> classroomsShool3 = new ArrayList<>();

        classroomsShool3.add(classroom7);
        classroomsShool3.add(classroom8);
        classroomsShool3.add(classroom9);

        return classroomsShool3;
    }

    public ArrayList<Classroom> getClassroomsWithMoreStudents(int noNumber) {
        ArrayList<Classroom> classrooms = new ArrayList<>();

        ArrayList<Classroom> allClassrooms = getAllClassrooms();

        for (Classroom classroom : allClassrooms) {
            if (classroom.getNoStudent() > noNumber) {
                classrooms.add(classroom);
            }
        }
        return classrooms;
    }
}
